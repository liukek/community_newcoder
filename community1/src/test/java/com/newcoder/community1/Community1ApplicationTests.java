package com.newcoder.community1;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newcoder.community1.entity.DiscussPost;
import com.newcoder.community1.entity.LoginTicket;
import com.newcoder.community1.entity.User;
import com.newcoder.community1.mapper.DiscussPostMapper;
import com.newcoder.community1.mapper.LoginTicketMapper;
import com.newcoder.community1.mapper.UserMapper;
import com.newcoder.community1.service.UserService;
import com.newcoder.community1.utils.CommunityUtil;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.Random;


@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = Community1Application.class)
class Community1ApplicationTests {
    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DiscussPostMapper discussPostMapper;
    @Autowired
    private LoginTicketMapper loginTicketMapper;

    @Test
    void getTest(){

        DiscussPost post = new DiscussPost();
        post.setUserId(1);
        System.out.println(post.getUserId());
        int id=1;
        User use = userMapper.getUserById(id);

      /*  List<DiscussPost> list = discussPostMapper.selectDiscussPosts(1111111, 0, 3);
        for (DiscussPost dis : list) {
            System.out.println(dis);
        }*/
        //System.out.println(use);
    }

    @Test
    public void testMypl(){
        User user = new User();
        user.setPassword("sfd666");
        //user.setCreateTime(new Date());
        //user.setStatus(0);
        user.setUsername("kkkkk5655");
        //userService.save(user);


        user.setSalt(CommunityUtil.generateUUID().substring(0,5));
        user.setPassword(CommunityUtil.md5(user.getPassword()+user.getSalt()));
        user.setType(0);
        user.setStatus(0);
        user.setActivationCode(CommunityUtil.generateUUID());
        user.setHeaderUrl(String.format("http://images.nowcoder.com/head/%dt.png", new Random().nextInt(1000)));//头像
        user.setCreateTime(new Date());
        //userMapper.insertUser(user);
        userMapper.insert(user);
        int id=user.getId();
        //user=userMapper.
    }

    @Test
    public void testActivition(){
        int re = userService.activition(1,"6666");
    }

    @Test
    public void testLoginTicket(){
        LoginTicket loginTicket = new LoginTicket();

        loginTicket.setStatus(1);

        //loginTicketMapper.insert(loginTicket);
        QueryWrapper queryWrapper = new QueryWrapper();

        queryWrapper.eq("user_id",12);
        loginTicketMapper.update(loginTicket,queryWrapper);

    }

    @Test
    public void udTicket(){
        QueryWrapper queryWrapper = new QueryWrapper();

        queryWrapper.eq("ticket","44ee3abbd9184696b3c1c5c364ee7c10");
        LoginTicket loginTicket = new LoginTicket();
        loginTicket.setStatus(1);
        //loginTicketMapper.updateStatus("",1);
        loginTicketMapper.update(loginTicket,queryWrapper);
    }

}
