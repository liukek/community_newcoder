package com.newcoder.community1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newcoder.community1.entity.Comment;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CommentMapper extends BaseMapper<Comment> {
}
