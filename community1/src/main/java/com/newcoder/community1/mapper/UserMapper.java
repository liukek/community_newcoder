package com.newcoder.community1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.newcoder.community1.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    User getUserById(int id);

    User selectByName(String username);

    User selectByEmail(String email);

    void insertUser(User user);
}
