package com.newcoder.community1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newcoder.community1.entity.LoginTicket;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface LoginTicketMapper extends BaseMapper<LoginTicket> {

    @Update({
            "<script>",
            "update login_ticket set status=#{status} where ticket=#{ticket} ",
            "<if test=\"ticket!=null\"> ",
            "and 1=1 ",
            "</if>",
            "</script>"
    })
    int updateStatus(String ticket, int i);
}
