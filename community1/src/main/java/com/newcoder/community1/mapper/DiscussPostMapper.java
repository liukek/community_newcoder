package com.newcoder.community1.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newcoder.community1.entity.DiscussPost;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DiscussPostMapper extends BaseMapper<DiscussPost> {

    List<DiscussPost> selectDiscussPosts(@Param("userId") int userId,@Param("page") int page,@Param("limit") int limit);
    // @Param注解用于给参数取别名,
    // 如果只有一个参数,并且在<if>里使用,则必须加别名.
    int selectDiscussPostRows(@Param("userId") int userId);
}
