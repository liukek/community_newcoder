package com.newcoder.community1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//@ComponentScan(basePackages = "com.newcoder")
@SpringBootApplication
public class Community1Application {

    public static void main(String[] args) {
        SpringApplication.run(Community1Application.class, args);
    }


}
