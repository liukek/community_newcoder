package com.newcoder.community1.controller;

import com.newcoder.community1.entity.User;
import com.newcoder.community1.service.FollowService;
import com.newcoder.community1.utils.CommunityUtil;
import com.newcoder.community1.utils.HostHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FollowController {

    @Autowired
    private HostHolder hostHolder;

    @Autowired
    private FollowService followService;

    //关注操作
    @PostMapping("/follow")
    @ResponseBody
    public String follow(int entityType,int entityId){
        User user = hostHolder.getUser();
        followService.follow(user.getId(),entityType,entityId);
        return CommunityUtil.getJSONString(0, "已关注");
    }
    //取消关注操作
    @PostMapping("/unfollow")
    @ResponseBody
    public String unfollow(int entityType,int entityId){
        User user = hostHolder.getUser();
        followService.unfollow(user.getId(),entityType,entityId);
        return CommunityUtil.getJSONString(0, "已取消关注");
    }

    //获取关注的列表
    @GetMapping("/followee/{userId}")
    public String getFollowees(@PathVariable("userId") int userId, Model model){

    }

    //获取粉丝列表
}
