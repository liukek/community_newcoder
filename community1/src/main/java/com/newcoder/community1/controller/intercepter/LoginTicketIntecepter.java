package com.newcoder.community1.controller.intercepter;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.newcoder.community1.entity.LoginTicket;
import com.newcoder.community1.entity.User;
import com.newcoder.community1.mapper.LoginTicketMapper;
import com.newcoder.community1.service.UserService;
import com.newcoder.community1.utils.CookieUtil;
import com.newcoder.community1.utils.HostHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

@Component
public class LoginTicketIntecepter implements HandlerInterceptor {

    @Autowired
    private LoginTicketMapper loginTicketMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private HostHolder hostHolder;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 从cookie中获取凭证
        String ticket = CookieUtil.getValue(request, "ticket");

        if (ticket != null) {
            // 查询凭证
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("ticket", ticket);
            LoginTicket loginTicket = loginTicketMapper.selectOne(queryWrapper);
            // 检查凭证是否有效
            if (loginTicket != null && loginTicket.getStatus() == 0 && loginTicket.getExpired().after(new Date())) {
                // 根据凭证查询用户
                User user = userService.getUserById(loginTicket.getUserId());
                // 在本次请求中持有用户
                hostHolder.setUser(user);
            }
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

        User user = hostHolder.getUser();
        if (user != null && modelAndView != null) {
            modelAndView.addObject("loginUser", user);
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        hostHolder.clear();
    }
}
