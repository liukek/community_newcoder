package com.newcoder.community1.controller;

import com.github.pagehelper.Page;
import com.newcoder.community1.entity.DiscussPost;
import com.newcoder.community1.entity.User;
import com.newcoder.community1.service.DiscussPostService;
import com.newcoder.community1.service.LikeService;
import com.newcoder.community1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.newcoder.community1.utils.CommunityConstant.ENTITY_TYPE_POST;

@Controller
public class HomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private DiscussPostService discussPostService;

    @Autowired
    private LikeService likeService;

    @RequestMapping(path = "/index", method = RequestMethod.GET)
    public String getIndexPage(Model model){

        // 方法调用钱,SpringMVC会自动实例化Model和Page,并将Page注入Model.
        // 所以,在thymeleaf中可以直接访问Page对象中的数据.

        List<DiscussPost> list = discussPostService.findDiscussPosts(1,0,10);
        List<Map<String, Object>> discussPosts = new ArrayList<>();



        //Page
        if(list!=null){
            for(DiscussPost post:list){
                Map<String, Object> map = new HashMap<>();
                User user = userService.getUserById(post.getUserId());
                map.put("post", post);
                map.put("user",user);

                long likeCount = likeService.findEntityLikeCount(ENTITY_TYPE_POST, post.getId());
                map.put("likeCount", likeCount);

                discussPosts.add(map);
            }
        }
        model.addAttribute("discussPosts",discussPosts);
        return "/index";
    }

    @RequestMapping(path = "/error", method = RequestMethod.GET)
    public String getErrorPage() {
        return "/error/500";
    }
}
