package com.newcoder.community1.service;

public interface FollowService {
    void follow(int id, int entityType, int entityId);

    void unfollow(int id, int entityType, int entityId);
}
