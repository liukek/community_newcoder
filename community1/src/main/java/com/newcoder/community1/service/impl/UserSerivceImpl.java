package com.newcoder.community1.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newcoder.community1.entity.LoginTicket;
import com.newcoder.community1.entity.User;
import com.newcoder.community1.mapper.LoginTicketMapper;
import com.newcoder.community1.mapper.UserMapper;
import com.newcoder.community1.service.UserService;
import com.newcoder.community1.utils.CommunityUtil;
import com.newcoder.community1.utils.MailClient;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.newcoder.community1.utils.CommunityConstant.*;

@Service
public class UserSerivceImpl extends ServiceImpl<UserMapper,User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private LoginTicketMapper loginTicketMapper;
    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private MailClient mailClient;

    @Value("${community.path.domain}")
    private String domain;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @Override
    public User getUserById(int id) {

        User user = userMapper.getUserById(id);
        return user;
    }

    //注册信息处理（判断填写信息）
    @Override
    public Map<String, Object> register(User user) {

        Map<String, Object> map = new HashMap<>();
        if (user == null) {
            throw new IllegalArgumentException("参数不能为空");
        }
        if (StringUtils.isEmpty(user.getUsername())) {
            map.put("usernameMsg","用户名不能为空");
            return map;
        }
        if (StringUtils.isEmpty(user.getPassword())) {
            map.put("passwordMsg", "密码不能为空!");
            return map;
        }
        if (StringUtils.isEmpty(user.getEmail())) {
            map.put("emailMsg", "邮箱不能为空!");
            return map;
        }

        // 验证账号
        User u = userMapper.selectByName(user.getUsername());
        if (u != null) {
            map.put("usernameMsg","该账号已注册");
            return map;
        }

        // 验证邮箱
        u = userMapper.selectByEmail(user.getEmail());
        if (u != null) {
            map.put("emailMsg","该邮箱已注册");
            return map;
        }

        /**
         * 注册用户
         */
        user.setSalt(CommunityUtil.generateUUID().substring(0,5));
        user.setPassword(CommunityUtil.md5(user.getPassword()+user.getSalt()));
        user.setType(0);
        user.setStatus(0);
        user.setActivationCode(CommunityUtil.generateUUID());
        user.setHeaderUrl(String.format("http://images.nowcoder.com/head/%dt.png", new Random().nextInt(1000)));//头像
        user.setCreateTime(new Date());
        //userMapper.insertUser(user);
        baseMapper.insert(user);

        // 激活邮件
        Context context = new Context();
        context.setVariable("email", user.getEmail());
        // http://localhost:8080/community/activation/101/code
        String url = domain + contextPath + "/activation/" + user.getId() + "/" + user.getActivationCode();
        context.setVariable("url", url);
        String content = templateEngine.process("/mail/activation", context);
        mailClient.sendMail(user.getEmail(), "激活账号", content);
        return map;
    }

    //激活操作
    @Override
    public int activition(int userId, String code) {

        User user = userMapper.getUserById(userId);
        if (user.getStatus() == 1) {
            return ACTIVITION_REPEAT;
        } else if (user.getActivationCode().equals(code)) {
            user.setStatus(1);
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("id", userId);
            userMapper.update(user,queryWrapper);
            return ACTIVITION_SUCCESS;
        } else {
            return ACTIVITION_FAILURE;
        }

    }

    @Override
    public Map<String, Object> login(String username, String password,int expiredSeconds) {
        Map<String, Object> map = new HashMap<>();
        // 空值处理
        if (StringUtils.isBlank(username)) {
            map.put("usernameMsg", "账号不能为空!");
            return map;
        }
        if (StringUtils.isBlank(password)) {
            map.put("passwordMsg", "密码不能为空!");
            return map;
        }

        //验证账号
        User user = userMapper.selectByName(username);
        if (user == null) {
            map.put("usernameMsg", "该账号不存在!");
            return map;
        }
        // 验证状态
        if (user.getStatus() == 0) {
            map.put("usernameMsg", "该账号未激活!");
            return map;
        }
        // 验证密码
        password = CommunityUtil.md5(password + user.getSalt());
        if (!user.getPassword().equals(password)) {
            map.put("passwordMsg", "密码不正确!");
            return map;
        }

        // 生成登录凭证
        LoginTicket loginTicket = new LoginTicket();
        loginTicket.setUserId(user.getId());
        loginTicket.setTicket(CommunityUtil.generateUUID());
        loginTicket.setStatus(0);
        loginTicket.setExpired(new Date(System.currentTimeMillis() + expiredSeconds * 1000));
        loginTicketMapper.insert(loginTicket);

        map.put("ticket", loginTicket.getTicket());
        return map;
    }

    public void logout(String ticket) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("ticket",ticket);
        LoginTicket loginTicket = new LoginTicket();
        loginTicket.setStatus(1);
        //loginTicketMapper.updateStatus("",1);
        loginTicketMapper.update(loginTicket,queryWrapper);
    }
}
