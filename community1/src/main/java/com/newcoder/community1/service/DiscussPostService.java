package com.newcoder.community1.service;

import com.newcoder.community1.entity.DiscussPost;

import java.util.List;

public interface DiscussPostService {

    List<DiscussPost> findDiscussPosts(int userId,int page,int limit);


    int addDiscussPost(DiscussPost post);

    DiscussPost findDiscussPostById(int discussPostId);
}
