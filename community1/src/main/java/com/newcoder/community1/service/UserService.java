package com.newcoder.community1.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.newcoder.community1.entity.User;

import java.util.Map;

public interface UserService extends IService<User> {
    User getUserById(int id);

    Map<String, Object> register(User user);

    int activition(int userId, String code);

    Map<String, Object> login(String username, String password,int expiredSeconds);

    void logout(String ticket);
}
