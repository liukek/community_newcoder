package com.newcoder.community1.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.newcoder.community1.entity.Comment;

import java.util.List;

public interface CommentService extends IService<Comment> {
    int findCommentCount(int entityTypeComment, int id);

    List<Comment> findCommentsByEntity(int entityType, int entityId);

    int addComment(Comment comment);
}
