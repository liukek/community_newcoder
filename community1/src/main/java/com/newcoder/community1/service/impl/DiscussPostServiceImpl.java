package com.newcoder.community1.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.newcoder.community1.entity.DiscussPost;
import com.newcoder.community1.mapper.DiscussPostMapper;
import com.newcoder.community1.service.DiscussPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DiscussPostServiceImpl implements DiscussPostService {

    @Autowired
    private DiscussPostMapper discussPostMapper;

    @Override
    public List<DiscussPost> findDiscussPosts(int userId, int page, int limit) {

        List<DiscussPost> list = discussPostMapper.selectDiscussPosts(userId,page,limit);
        return list;
    }

    @Override
    public int addDiscussPost(DiscussPost post) {

        return discussPostMapper.insert(post);
    }

    @Override
    public DiscussPost findDiscussPostById(int discussPostId) {
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("id", discussPostId);
        return discussPostMapper.selectOne(wrapper);
    }
}
