package com.newcoder.community1.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newcoder.community1.entity.Comment;
import com.newcoder.community1.entity.DiscussPost;
import com.newcoder.community1.mapper.CommentMapper;
import com.newcoder.community1.mapper.DiscussPostMapper;
import com.newcoder.community1.service.CommentService;
import com.newcoder.community1.utils.SensitiveFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

import java.util.ArrayList;
import java.util.List;

import static com.newcoder.community1.utils.CommunityConstant.ENTITY_TYPE_POST;

@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper,Comment> implements CommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private DiscussPostMapper discussPostMapper;

    @Autowired
    private SensitiveFilter sensitiveFilter;

    @Override
    public int findCommentCount(int entityTypeComment, int id) {

        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("entity_type",entityTypeComment)
                .eq("entity_id",id)
                .eq("status",0);

        int count = commentMapper.selectCount(queryWrapper);
        return count;
    }

    @Override
    public List<Comment> findCommentsByEntity(int entityType, int entityId) {
        //List<Comment> list = new ArrayList<>();
        QueryWrapper<Comment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("entity_type",entityType)
                .eq("entity_id",entityId)
                .eq("status",0);
        List<Comment> list = commentMapper.selectList(queryWrapper);
        return list;
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public int addComment(Comment comment) {
        if (comment == null) {
            throw new IllegalArgumentException("参数不能为空!");
        }

        // 添加评论
        comment.setContent(HtmlUtils.htmlEscape(comment.getContent()));
        comment.setContent(sensitiveFilter.filter(comment.getContent()));
        int rows = commentMapper.insert(comment);

        // 更新帖子评论数量
        if (comment.getEntityType() == ENTITY_TYPE_POST) {
            int count = findCommentCount(comment.getEntityType(), comment.getEntityId());
            DiscussPost discussPost = new DiscussPost();
            discussPost.setId(comment.getEntityId());
            discussPost.setCommentCount(count);
            discussPostMapper.updateById(discussPost);
        }

        return rows;
    }


}
